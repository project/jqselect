/*************************************************************************************************/
/*                                                                                               */
/*                                       jqselect                                                */
/*                                                                                               */
/* By Mixel Kiemen 12-2006                                                                       */
/* Created and used for the KNOSOS project (for a specific implementation see node_access)       */
/*                                                                                               */
/*************************************************************************************************/


//we need a simple way to get the element, byId seams the most easiest
//we use a "scope name"  make sure we can have more jqselects in on page and a "find name" to get that part we are looking for
function jqselect_find(sname, fname)
{
 return document.getElementById(sname+'_jqs_'+fname);
}

//init the jsqselect
//get data: suggestions, selections and free text => not required, some may be not existing (like only free_select or only suggestions)
//get the buttons, add & remove and add the jquery function to it.
function jqselect_init(sname) 
{	
  var suggestions = jqselect_find(sname,'suggestions');
  var selections = jqselect_find(sname,'selections');
  var free_select = jqselect_find(sname,'free_select');

  var add = jqselect_find(sname, 'add');
  var remove = jqselect_find(sname, 'remove');

  jqselect_init_check_selections(sname, selections);

  $(add).click(function()
    {	 		
      jqselect_callapi(sname, 'before_add', -1, '', '');
      jqselect_add(sname, suggestions, selections, free_select); 
      jqselect_callapi(sname, 'after_add', -1, '','');	
    });
  $(remove).click(function()
    {
    jqselect_callapi(sname, 'before_remove', -1,'','');
      jqselect_remove(sname, suggestions, selections); 
      jqselect_callapi(sname, 'after_remove', -1, '','');
    });	
}

function jqselect_init_check_selections(sname, selections)
{
  var checklist = jqselect_find(sname,'checklist').options;

  for (var i = 0; i < selections.length; i++)
    if(jqselect_init_check(selections[i].text, checklist))
        $(selections[i]).attr('class','suggestion'); //add this class for the remove => jqselect_remove_or_replace
}

function jqselect_init_check(value, checklist) //very simple array search, should be replaced by the right jquery method
{
  for (var i = 0; i < checklist.length; i++)
    if (checklist[i].text == value) 
      return true;
  return false;   
}

//a hook_function usfull if you like to have other operations evalueted when the aad or remove are used.
//see node_access for an example
function jqselect_callapi(sname, op, id, value, tag) 
{

  var check_callapi = jqselect_find(sname,'check_callapi');

  if (typeof($(check_callapi).val()) != 'undefined')
    eval($(check_callapi).val()+'_jqselectAPI(\''+sname+'\',\''+op+'\','+id+',\''+value+'\',\''+tag+'\')');	
}

/********************************************************/
/*                        adding                        */
/********************************************************/

//check for select in both cases: sugestion list and free text
function jqselect_add(sname, suggestions, selections, free_select)
{		
  if($(free_select).val())
    jqselect_add_free_select(sname, suggestions, selections, free_select);

  jqselect_add_recursieve_suggestions(sname, suggestions, selections);
}

//remove recursively the selections items  
function jqselect_add_recursieve_suggestions(sname, suggestions, selections)
{	
  if (suggestions.selectedIndex > -1)
  {	
    jqselect_add_suggestions(sname, suggestions, selections, suggestions.selectedIndex);
    jqselect_add_recursieve_suggestions(sname, suggestions, selections);
  }
}

//do the one transformation out of the sugestion list and into the select list
function jqselect_add_suggestions(sname, suggestions, selections, thisindex)
{
  var opt = suggestions.options[thisindex];
  jqselect_callapi(sname, 'add', $(opt).val(), opt.text, '');
  $(opt).attr('class','suggestion'); //add this class for the remove => jqselect_remove_or_replace
  $(selections).append($(opt));
}

//remove the free text and add it to select list. Do a check to see if the item is suggested
//when there is a callbackUri we shall check for id (ajax).
function jqselect_add_free_select(sname, suggestions, selections, free_select)
{	
  var thisval = $(free_select).val();
 $(free_select).val('');
	
  var thisindex = jqselect_free_is_suggestion(suggestions, thisval);
	
  if(thisindex != -1)
    jqselect_add_suggestions(sname, suggestions, selections, thisindex);
  else 
  {
    var callbackUri = jqselect_find(sname, 'callback');
    var uri = $(callbackUri).val()+thisval;
	

    if (typeof($(callbackUri).val()) != 'undefined'){
      $.ajax(
	  {
        type: "GET",
        url:  uri,
        
        success: function (thisid) 
        {
        
          jqselect_adding_free_select(thisid, thisval, selections, sname);
          
        },
        error: function (xmlhttp) 
        {
          alert('An HTTP error '+ xmlhttp.status +' occured.\n'+ uri);
        }
      });
      //$('#selected_jqs_list').html('ajax done ');
    }
    else{
      jqselect_adding_free_select(-1, thisval, selections,sname);
      }  
  }
}

function jqselect_adding_free_select(thisid, thisval, selections, sname) 
{

  var option = document.createElement('option');

  if(thisid == 'false')
   return alert('Input error '+sname+' \"'+ thisval+'\" not found.');

  if(thisid != -1)
    $(option).attr('value',thisid); 

  jqselect_callapi(sname, 'add', thisid, thisval, '');
  option.innerHTML= thisval;
  $(selections).append($(option));
  jqselect_callapi(sname, 'after_add', thisid, thisval, '');
}

function jqselect_free_is_suggestion(suggestions, name) 
{
  for(var i = 0; i < suggestions.length; i++)
    if(name == suggestions[i].text)
      return i;

  return -1; 
}

/********************************************************/
/*                        removing                      */
/********************************************************/

//For each selections item to remove we do a check
function jqselect_remove(sname, suggestions, selections)
{
  if (selections.selectedIndex > -1)
  {
    var opt = selections.options[selections.selectedIndex];
    jqselect_callapi(sname, 'remove', selections.selectedIndex, opt.text, '');
    jqselect_remove_or_replace($(opt), suggestions);
    jqselect_remove(sname, suggestions, selections);
  }
}
//When the item was part of the sugesestion list we have to add again to that list
function jqselect_remove_or_replace(option, suggestions)
{	
  if(option.attr('class') == 'suggestion')
    $(suggestions).append(option);
  else
    option.remove();
}